# Twitch Chatbot 

### Sending commands based on input from bot that listens to events

Here you will find a simple Python chatbot using IRC that can help demonstrate how to interact with chat on Twitch.

## Installation
After you have cloned this repository, use pip or easy_install to install the IRC library.

## Requirements Install 
<br>

```sh
$ pip install irc
```

## Usage
To run the chatbot, you will need to provide an OAuth access token with the chat_login scope.  You can reference an authentication sample to accomplish this, or simply use the [Twitch Chat OAuth Password Generator](http://twitchapps.com/tmi/).

```sh
$ python chatbot.py <username> <client id> <token> <channel>
```
<br>

* Username - The username of the chatbot
* Client ID - Your registered application's Client ID to allow API calls by the bot
* Token - Your OAuth Token
* Channel - The channel your bot will connect to


## Next Iterations

* Listen for multiple events and act accordingly
* Make bot connect as different bots and send command with the corresponding one
* Add multi-channel support on connection
* Allow Settings to be saved into a file and be added automatically not everytime script runs
