# -*- coding: utf-8 -*-

import sys
import irc.bot
import requests
import sys
import os.path
import os
import time
import io

class TwitchBot(irc.bot.SingleServerIRCBot):
    def __init__(self, username, client_id, token, channel):
        # Set up the settings
        self.client_id = client_id
        self.token = token
        self.channel = '#' + channel

        # Get the channel id, we will need this for v5 API calls
        url = 'https://api.twitch.tv/kraken/users?login=' + channel
        headers = {'Client-ID': client_id, 'Accept': 'application/vnd.twitchtv.v5+json'}
        r = requests.get(url, headers=headers).json()
        self.channel_id = r['users'][0]['_id']

        # Create IRC bot connection
        server = 'irc.chat.twitch.tv'
        port = 6667
        print 'Connecting to ' + server + ' on port ' + str(port) + '...'
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port, 'oauth:'+token)], username, username)
        

    def on_welcome(self, c, e):
        print 'Joining ' + self.channel

        # You must request specific capabilities before you can use them
        c.cap('REQ', ':twitch.tv/membership')
        c.cap('REQ', ':twitch.tv/tags')
        c.cap('REQ', ':twitch.tv/commands')
        c.join(self.channel)

    
    def on_pubmsg(self, c, e):
        # Check file to see if a song request is performed
        check_file = os.path.exists('/Users/kadosanakyklosis/Downloads/twitch-pubsub/file_1.txt')
        
        if (check_file) == True:            
            cmd = e.arguments[0].split(' ')[0][1:]        
            self.do_command(e, cmd)
            return
        return

    # do command function triggered to send the command
    def do_command(self, e, cmd):
        c = self.connection
        starttime = time.time() 
        with io.open('path/to/file/file_1.txt') as f:
            s = " ".join([x.strip() for x in f])
            message = "!sr " + s
            print ("Sending song request commands for : " + s)
            c.privmsg(self.channel, message)
            time.sleep(20.0 - ((time.time() - starttime) % 20.0))
  
def main():
    if len(sys.argv) != 5:
        print("Usage: twitchbot <username> <client id> <token> <channel>")
        sys.exit(1)

    username  = sys.argv[1]
    client_id = sys.argv[2]
    token     = sys.argv[3]
    channel   = sys.argv[4]

    # Starting the bot
    
    bot = TwitchBot(username, client_id, token, channel)
    bot.start()

if __name__ == "__main__":
    main()
